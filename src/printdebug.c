/*
    Debugging values:
        1 = Development debugging setting. Set to constant 1 (cant change) and will use TRACE functionaltiy.
        0 = Release debug setting. Will not log to file by default.
        anyother value = Release debug setting. Will log to DEBUG_FILE instead of TRACE()


*/
#include "../include/printdebug.h"

#ifndef __DEBUG__
    #define __DEBUG__ 0
#endif

void printdebug(char *Message) {
    char *DEBUG_FILE = "/t/debug.log";

    switch (__DEBUG__) {
        case 1:
            TRACE(0,0,Message);
            break;
        case 2:
            //write to file
            break;
        default:
            // do nothing
            break;

    }



}