//
// Created by cheerio on 6/1/2019.
//

#include "../include/hardware.h"

struct simcard {
    int inserted;
    int locked;
    int service_enabled;
    char *imei;
};
struct sdcard {
    int inserted;
    int encrypted;
    int compressed;
    double capacity;
    double space_free;
    double space_used;
    char *mount_point;
    int unmounting;
};
struct network {
    char *apn;
    int connected;
    int status;
    int registered;
    int in_service;
    char *ip_address;
    double latency;
    int socket_operations_available;
};
struct device {
    int sleep_state;
    int battery_capacity;
    int power_mode;
    int charging_indicator;
    struct simcard *simcard;
    struct sdcard *sdcard;
    struct network *network;
};
#include <api_os.h>

struct device *device_alloc(void) {
    struct device *device;
    device = OS_Alloc(sizeof(*device));
    if (device == NULL)
        return NULL;
    OS_Memset(device, 0, sizeof(*device));

    if ( (device->sdcard = sdcard_alloc()) == NULL ) {
        OS_Free(device);
        return NULL;
    }
    if ( (device->simcard = simcard_alloc()) == NULL ) {
        OS_Free(device);
        return NULL;
    }
    if ( (device->network = network_alloc()) == NULL ) {
        OS_Free(device);
        return NULL;
    }

    return device;
}

struct simcard *simcard_alloc(void) {
    struct simcard *simcard;
    simcard = OS_Alloc(sizeof(*simcard));
    if (simcard == NULL)
        return NULL;
    OS_Memset(simcard, 0, sizeof(*simcard));

    return simcard;
}
struct sdcard *sdcard_alloc(void) {
    struct sdcard *sdcard;
    sdcard = OS_Alloc(sizeof(*sdcard));
    if (sdcard == NULL)
        return NULL;
    OS_Memset(sdcard, 0, sizeof(*sdcard));

    return sdcard;
}
struct network *network_alloc(void) {
    struct network *network;
    network = OS_Alloc(sizeof(*network));
    if (network == NULL)
        return NULL;
    OS_Memset(network, 0, sizeof(*network));

    return network;
}