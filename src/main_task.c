
#include "../include/main_task.h"

void gps_MainTask(void *pData) // Application Main Task: Event Loop Dispatcher
{
    API_Event_t* event=NULL;
    
    TIME_SetIsAutoUpdateRtcTime(true);
    
    //open UART1 to print NMEA infomation
    UART_Config_t config = {
        .baudRate = UART_BAUD_RATE_115200,
        .dataBits = UART_DATA_BITS_8,
        .stopBits = UART_STOP_BITS_1,
        .parity   = UART_PARITY_NONE,
        .rxCallback = NULL,
        .useEvent   = true
    };
    UART_Init(UART1,config);

    //Create UART1 send task and location print task
    OS_CreateTask(gps_location_tracking_task,
            NULL, NULL, GPSTRACKER_TASK_STACK_SIZE, GPSTRACKER_TASK_PRIORITY, 0, 0, GPSTRACKER_TASK_NAME);

    //Wait event
    while(1)
    {
        if(OS_WaitEvent(handle_gpsMainTask, (void**)&event, OS_TIME_OUT_WAIT_FOREVER))
        {
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}


