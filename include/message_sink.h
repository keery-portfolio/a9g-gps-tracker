//
// Created by cheerio on 6/1/2019.
//

#ifndef GPS_TRACKER_MESSAGE_SINK_H
#define GPS_TRACKER_MESSAGE_SINK_H

#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "buffer.h"
#include "gps_parse.h"
#include "math.h"
#include "gps.h"
#include "api_hal_pm.h"
#include "time.h"
#include "api_info.h"
#include "assert.h"
#include "api_socket.h"
#include "api_network.h"
bool networkFlag;
bool isGpsOn;
void MessageSink(API_Event_t *);
#endif //GPS_TRACKER_MESSAGE_SINK_H
