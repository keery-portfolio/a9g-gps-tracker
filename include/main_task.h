#include <api_os.h>
#include <api_event.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "buffer.h"
#include "gps_parse.h"
#include "math.h"
#include "gps.h"
#include "api_hal_pm.h"
#include "time.h"
#include "api_info.h"
#include "assert.h"
#include "api_socket.h"
#include "api_network.h"
#include "location_task.h"
#define GPSTRACKER_TASK_STACK_SIZE    (2048 * 2)
#define GPSTRACKER_TASK_PRIORITY      0
#define GPSTRACKER_TASK_NAME          "GPS Location Tracking Task"
void gps_MainTask(void*);
HANDLE  handle_gpsMainTask();