#include <api_gps.h>
#include <gps_parse.h>
#include <gps.h>
#include <api_network.h>
#include <minmea.h>
#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include <buffer.h>
#include <gps_parse.h>
#include <math.h>
#include <gps.h>
#include <api_hal_pm.h>
#include <time.h>
#include <api_info.h>
#include <assert.h>
#include <api_socket.h>
#include <api_network.h>
#include "printdebug.h"
#include "http.h"

#define SERVER_IP   "ss.neucrack.com"
#define SERVER_PORT  8082

#define GPS_NMEA_LOG_FILE_PATH "/t/gps_nmea.log"



#define MAIN_TASK_STACK_SIZE    (2048 * 2)
#define MAIN_TASK_PRIORITY      0
#define MAIN_TASK_NAME          "GPS Location Tracking Task"
uint8_t buffer[1024],buffer2[400];

bool networkFlag;
bool isGpsOn;
void MessageSink(API_Event_t *);
void gps_location_tracking_task(void *);
//