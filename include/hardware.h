//
// Created by cheerio on 6/1/2019.
//

#ifndef GPS_TRACKER_HARDWARE_H
#define GPS_TRACKER_HARDWARE_H
struct simcard;
struct network;
struct sdcard;
struct device;
struct device *device_alloc(void);
struct simcard *simcard_alloc(void);
struct sdcard *sdcard_alloc(void);
struct network *network_alloc(void);
#endif //GPS_TRACKER_HARDWARE_H
