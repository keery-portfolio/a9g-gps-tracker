//
// Created by cheerio on 6/1/2019.
//
/*
    Debugging values:
        1 = Development debugging setting. Set to constant 1 (cant change) and will use TRACE functionaltiy.
        0 = Release debug setting. Will not log to file by default.
        anyother value = Release debug setting. Will log to DEBUG_FILE instead of TRACE()


*/
#include <api_debug.h>
//#include "api_debug.h"
#ifndef GPS_TRACKER_PRINTDEBUG
#define GPS_TRACKER_PRINTDEBUG

#define Trace(a, b) (printdebug( (char *)b));
void printdebug(char *);

#endif //GPS_TRACKER_ABC_H



